import {useEffect, useState} from "react";
import './Shop.css';
const Shop = () => {
    const [product, setProduct] = useState({});
    const [id, setId] = useState('1');
    const [query,setQuery] = useState('');
    useEffect(() => {
    getProductData(id).then(productData =>{
        setProduct(productData);});      
    }, [id]);

    const changeId = () =>{
        setId(query);
    };
    const changeQuery = (e) =>{
        setQuery(e.target.value);
    };

    const getProductData = async(id) =>{
        const request =  await fetch(`https://fakestoreapi.com/products/${id}`);
        return await request.json();
    };
    return (
        <div className={"searchBar"}>
            <input type={"text"} value={query} onChange={changeQuery}/> 
            <button value={"search"}  onClick={changeId} >search</button>
            <div className={"searchBar__title"}>
            {product.title && <p>{`This Item is ${product.title} and it's price is ${product.price}`}</p> }
            </div>
        </div>
        
    );
};

export default Shop
